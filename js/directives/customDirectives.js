app.directive('customDirective', function() {
    return {
        restrict: 'E',
        template: '<div>Custom Directive</div>'
    };
});