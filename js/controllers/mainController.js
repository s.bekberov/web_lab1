app.controller('mainController', function($scope) {
    $scope.title = 'Галерея фотографій';
    $scope.photos = [
        {url: 'https://via.placeholder.com/150', title: 'Фото 1', description: 'Опис фото 1'},
        {url: 'https://via.placeholder.com/150', title: 'Фото 2', description: 'Опис фото 2'},
        {url: 'https://via.placeholder.com/150', title: 'Фото 3', description: 'Опис фото 3'}
    ];

    $scope.addPhoto = function() {
        if ($scope.newPhoto.url && $scope.newPhoto.title && $scope.newPhoto.description) {
            $scope.photos.push($scope.newPhoto);
            $scope.newPhoto = {};
        }
    };

    $scope.removePhoto = function(index) {
        $scope.photos.splice(index, 1);
    };

    $scope.viewMode = 'list';

    $scope.toggleView = function() {
        $scope.viewMode = ($scope.viewMode === 'list') ? 'grid' : 'list';
    };

    $scope.sortType = 'title';
    $scope.sortReverse = false;
});